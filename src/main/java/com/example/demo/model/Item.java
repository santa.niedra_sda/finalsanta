package com.example.demo.model;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private double price;

    @OneToOne(fetch = FetchType.EAGER)
    private Category category;

    public Item() {
    }

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

}
