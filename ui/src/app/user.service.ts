import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from "rxjs";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url= "http:/localhoast:8080/user"
  }

  public save (user: User) {
    return this.http.post<User>(this.url, user);
  }
}
