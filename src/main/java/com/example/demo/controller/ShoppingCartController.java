package com.example.demo.controller;

import com.example.demo.model.Item;
import com.example.demo.model.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.demo.service.ShoppingService;

@CrossOrigin
public class ShoppingCartController {

    private ShoppingService shoppingService;

    @Autowired
    public ShoppingCartController(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @GetMapping("add/{id}")
    public Item addItemToCart(@PathVariable("id") Long id){
        return shoppingService.addItemToCart(id);
    }

    @GetMapping("getCart")
    public ShoppingCart getCart(){
        return shoppingService.getShoppingCart();
    }
}
