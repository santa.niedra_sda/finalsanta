import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateUserComponent} from "./create-user/create-user.component";
import {HomeComponent} from "./home/home.component";
import {NavigationComponent} from "./navigation/navigation.component";

const routes: Routes = [
  {
    path: 'login',
    component: CreateUserComponent
  },

  {
    path: 'home',
    component: HomeComponent
  },

  {
    path: '',
    component: NavigationComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
