package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    @ManyToMany (fetch = FetchType.EAGER)
    private List<Item> items;

    public ShoppingCart() {
    }

    public ShoppingCart(User user, List<Item> items) {
        this.user = user;
        this.items = items;
    }


}
