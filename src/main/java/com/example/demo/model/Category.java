package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    String name;

    @ManyToOne
    Category parentCategory;

    @OneToMany
    List<Category> childCategory;


}
