package com.example.demo.service;

import com.example.demo.model.Item;
import com.example.demo.model.ShoppingCart;
import com.example.demo.model.User;
import com.example.demo.repository.ItemRepository;
import com.example.demo.repository.ShoppingCartRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class ShoppingService {

    private UserRepository userRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private ItemRepository itemRepository;

    @Autowired
    public ShoppingService(UserRepository userRepository,
                           ShoppingCartRepository shoppingCartRepository,
                           ItemRepository itemRepository) {
        this.userRepository = userRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.itemRepository = itemRepository;
    }

    public Item addItemToCart(Long id) {
        ShoppingCart shoppingCart = getShoppingCart();
        Item item = itemRepository.findById(id).get();
        shoppingCart.getItems().add(item);
        shoppingCartRepository.save(shoppingCart);
        return item;
    }
    public ShoppingCart getShoppingCart() {
        String name =
                SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findByName(name);
        ShoppingCart shoppingCart = shoppingCartRepository.findByUser_id(user.getId());
        return shoppingCart;
    }

}
